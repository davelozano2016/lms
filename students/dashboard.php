<?php include_once '../header.php';?>

<body>

	<!-- Main navbar -->
	<?php include_once '../navigation.php';?>
	<!-- /main navbar -->


	<!-- Page content -->
	<div class="page-content">

		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Inner content -->
			<div class="content-inner">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content container d-sm-flex">
						<div class="page-title">
							<h4><span class="font-weight-semibold">Dashboard</span></h4>
						</div>

						<!-- <div class="my-sm-auto ml-sm-auto mb-3 mb-sm-0">
							<button type="button" class="btn btn-primary w-100 w-sm-auto">Button</button>
						</div> -->
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content container pt-0">

					<!-- Basic card -->
					<div class="row">
						<div class="col-md-8 col-12">
							<div class="card">
								<div class="card-header">
									<h5 class="card-title">Basic card</h5>
								</div>

								<div class="card-body">
									<h6 class="font-weight-semibold">Start your development with no hassle!</h6>

								</div>
							</div>
						</div>
						<div class="col-md-4 col-12">
							<?php include_once 'sidebar.php' ?>
						</div>
					</div>
					<!-- /basic card -->


				</div>
				<!-- /content area -->


				<?php include_once '../footer.php';?>

			</div>
			<!-- /inner content -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

</body>

</html>