<div class="row">
    <div class="col-md-12">
        <form method="POST">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <label for="">Search</label>
                        <input type="text" class="form-control">
                    </div>

                    <div class="float-right">
                        <button class="btn btn-primary btn-sm">Go</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title">Modules <span class="small text-muted">5 items</span> </h5>
            </div>

            <div class="card-body">
                <?php for($i=1;$i<=5;$i++) { ?>
                    <div class="media">
                        <div class="mr-3">
                            <a href="#"><i class="icon-file-play text-warning icon-2x mt-1"></i></a>
                        </div>

                        <div class="media-body">
                            <h6 class="media-title font-weight-semibold"><a href="#" class="text-body">Module <?=$i++?></a></h6>
                            Strove the darn hey as far oh alas and yikes and gosh knitted this slept via gerbil baneful
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title">Recent on going modules <span class="small text-muted">5 items</span> </h5>
            </div>

            <div class="card-body">
                <?php for($i=1;$i<=5;$i++) { ?>
                    <div class="media">
                        <div class="mr-3">
                            <a href="#"><i class="icon-file-play text-warning icon-2x mt-1"></i></a>
                        </div>

                        <div class="media-body">
                            <h6 class="media-title font-weight-semibold"><a href="#" class="text-body">Module <?=$i++?></a></h6>
                            Strove the darn hey as far oh alas and yikes and gosh knitted this slept via gerbil baneful...
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title">Recent completed modules <span class="small text-muted">5 items</span></h5>
            </div>

            <div class="card-body">
                <?php for($i=1;$i<=5;$i++) { ?>
                    <div class="media">
                        <div class="mr-3">
                            <a href="#"><i class="icon-file-play text-warning icon-2x mt-1"></i></a>
                        </div>

                        <div class="media-body">
                            <h6 class="media-title font-weight-semibold"><a href="#" class="text-body">Module <?=$i++?></a></h6>
                            Strove the darn hey as far oh alas and yikes and gosh knitted this slept via gerbil baneful
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>