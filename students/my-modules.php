<?php include_once '../header.php';?>

<body>

	<!-- Main navbar -->
	<?php include_once '../navigation.php';?>
	<!-- /main navbar -->


	<!-- Page content -->
	<div class="page-content">

		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Inner content -->
			<div class="content-inner">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content container d-sm-flex">
						<div class="page-title">
							<h4><span class="font-weight-semibold"><?=urldecode(decryption($_GET['list']))?></span></h4>
						</div>

						<!-- <div class="my-sm-auto ml-sm-auto mb-3 mb-sm-0">
							<button type="button" class="btn btn-primary w-100 w-sm-auto">Button</button>
						</div> -->
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content container pt-0">

					<!-- Basic card -->
					<div class="row">
						<div class="col-md-8 col-12 mb-3">
							<div id="accordion-group">
								<?php foreach(get_unit_by_module(urldecode(decryption($_GET['list']))) as $unit) { ?>
									<div class="card mb-0 rounded-bottom-0">
										<div class="card-header">
											<h6 class="card-title">
												<a data-toggle="collapse" class="text-body"
													href="#accordion_<?=md5(base64_encode($unit['lesson_id']))?>"><?=$unit['lesson']?></a>
											</h6>
										</div>

										<div id="accordion_<?=md5(base64_encode($unit['lesson_id']))?>" class="collapse"
											data-parent="#accordion-group">
											<div class="card-body">
												Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
												richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard
												dolor brunch.
											</div>
										</div>
									</div>
								<?php } ?>
							</div>
							<!-- /accordion widget -->

						</div>

						<div class="col-md-4 col-12">
							<?php include_once 'sidebar.php' ?>
						</div>
					</div>
					<!-- /basic card -->


				</div>
				<!-- /content area -->


				<?php include_once '../footer.php';?>

			</div>
			<!-- /inner content -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

</body>

</html>