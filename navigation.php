<div class="navbar navbar-expand-xl navbar-light navbar-static px-0">
		<div class="container px-0">
			<div class="d-flex flex-1 pl-3">
				<div class="navbar-brand wmin-0 mr-1">
					<a href="index.html" class="d-inline-block">
						<img src="assets/images/logo_dark.png" class="d-none d-sm-block" alt="">
						<img src="assets/images/logo_icon_dark.png" class="d-sm-none" alt="">
					</a>
				</div>
			</div>

			<div class="d-flex w-100 w-xl-auto overflow-auto overflow-xl-visible scrollbar-hidden border-top border-top-xl-0 order-1 order-xl-0">
				<ul class="navbar-nav navbar-nav-underline flex-row text-nowrap mx-auto">
					<li class="nav-item">
						<a href="index.php" class="navbar-nav-link">
							Dashboard
						</a>
					</li>

					<li class="nav-item nav-item-dropdown-xl dropdown">
						<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							My Modules
						</a>
						<div class="dropdown-menu">
							<?php $query = $db->query("SELECT *, COUNT(l.subject_id) as items FROM courses as c INNER JOIN subjects as s ON c.id = s.course_id LEFT JOIN lessons as l ON s.id = l.subject_id GROUP BY l.subject_id ORDER BY s.id ASC"); ?>
							<?php foreach($query as $data) { ?>
								<a href="my-modules.php?list=<?=urlencode(encryption($data['subject']))?>" class="dropdown-item">
									<span style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;"><?=$data['subject']?>  &nbsp; &nbsp; </span>
									<span class="badge badge-primary ml-auto badge-pill"><?=$data['items']?></span>
								</a>
							<?php } ?>
						</div>
					</li>
				</ul>
			</div>

			<div class="d-flex flex-xl-1 justify-content-xl-end order-0 order-xl-1 pr-3">
				<ul class="navbar-nav navbar-nav-underline flex-row">
					<li class="nav-item">
						<a href="#" class="navbar-nav-link navbar-nav-link-toggler">
							<i class="icon-bell2"></i>
							<span class="badge badge-mark border-pink bg-pink"></span>
						</a>
					</li>
			
					<li class="nav-item nav-item-dropdown-xl dropdown dropdown-user h-100">
						<a href="#" class="navbar-nav-link navbar-nav-link-toggler d-flex align-items-center h-100 dropdown-toggle" data-toggle="dropdown">
							<img src="../assets/images/placeholders/placeholder.jpg" class="rounded-circle mr-xl-2" height="38" alt="">
							<span class="d-none d-xl-block">User</span>
						</a>
			
						<div class="dropdown-menu dropdown-menu-right">
							<a href="#" class="dropdown-item">My Profile</a>
							<a href="#" class="dropdown-item">My Modules</a>

							<a href="#" class="dropdown-item">Logout</a>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>