<!-- Footer -->
<div class="navbar navbar-expand-lg navbar-light border-bottom-0 border-top">
					<div class="container px-lg-3">
						<div class="text-center d-lg-none w-100">
							<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-third">
								<i class="icon-menu mr-2"></i>
								Bottom navbar
							</button>
						</div>

						<div class="navbar-collapse collapse" id="navbar-third">
							<span class="navbar-text">
								&copy; 2015 - 2018. <a href="#">Limitless Web App Kit</a> by <a href="https://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
							</span>

							<ul class="navbar-nav ml-lg-auto">
								<li class="nav-item">
									<a href="#" class="navbar-nav-link">Link</a>
								</li>
								<li class="nav-item">
									<a href="#" class="navbar-nav-link"><i class="icon-stack2"></i></a>
								</li>
								<li class="nav-item dropup">
									<a href="#" class="navbar-nav-link" data-toggle="dropdown">
										<i class="icon-more2 d-none d-lg-inline-block"></i>
										<span class="d-lg-none">Menu</span>
									</a>

									<div class="dropdown-menu dropdown-menu-right">
										<a href="#" class="dropdown-item">Item 1</a>
										<a href="#" class="dropdown-item">Item 2</a>
										<a href="#" class="dropdown-item">Item 3</a>
										<div class="dropdown-divider"></div>
										<a href="#" class="dropdown-item">Item 4</a>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- /footer -->